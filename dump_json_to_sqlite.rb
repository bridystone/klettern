require 'net/http'
require 'json'
require 'sqlite3'

db = SQLite3::Database.open 'brutscher_20200817.db'
db.execute "CREATE TABLE IF NOT EXISTS gebiete(gebietid INT, sektornr INT, sektor_ID INT, sektorname_d TEXT, sektorname_cz TEXT)"
db.execute "CREATE TABLE IF NOT EXISTS gipfel(gipfel_ID INT, gipfelnr INT, gipfelname_d TEXT, gipfelname_cz TEXT, status TEXT, typ TEXT, vgrd TEXT, ngrd TEXT, posfehler INT, schartenhoehe INT, talhoehe INT, sektorid INT)"
db.execute "CREATE TABLE IF NOT EXISTS wege(weg_ID INT, gipfelid INT, schwierigkeit TEXT, erstbegvorstieg TEXT, erstbegnachstieg TEXT, erstbegdatum TEXT, ringzahl INT, wegbeschr_d TEXT, wegbeschr_cz TEXT, kletterei TEXT, wegname_d TEXT, wegname_cz TEXT, wegstatus TEXT, wegnr INT)"
db.execute "CREATE TABLE IF NOT EXISTS kommentare (komment_ID INT, userid INT, datum TEXT, adatum TEXT, wegid INT, sektorid INT, gebietid INT, qual INT, sicher INT, nass INT, kommentar TEXT, gipfelid INT, schwer TEXT, geklettert INT, begehung INT)"

#empty database
db.execute "DELETE FROM gebiete"
db.execute "DELETE FROM gipfel"
db.execute "DELETE FROM wege"
db.execute "DELETE FROM kommentare"

url_gebiet = 'http://db-sandsteinklettern.gipfelbuch.de/jsonteilgebiet.php?app=yacguide&gebietid=19'
uri_gebiet = URI(url_gebiet)
response_gebiet = Net::HTTP.get(uri_gebiet)
obj_gebiet = JSON.parse(response_gebiet)

obj_gebiet.each do |gebiet|
    puts gebiet["gebietid"] #19=ELBI
    puts gebiet["sektornr"] #local numbering
    puts gebiet["sektor_ID"] # global numbering -> IMPORTANT
    puts gebiet["sektorname_d"]
    puts gebiet["sektorname_cz"]
    db.execute "INSERT INTO gebiete VALUES (?,?,?,?,?)", gebiet["gebietid"], gebiet["sektornr"] ,gebiet["sektor_ID"], gebiet["sektorname_d"], gebiet["sektorname_cz"]

    # GIPFEL
    uri_gipfel = URI("http://db-sandsteinklettern.gipfelbuch.de/jsongipfel.php?app=yacguide&sektorid=" + gebiet["sektor_ID"])
    response_gipfel = Net::HTTP.get(uri_gipfel)
    obj_gipfel = JSON.parse(response_gipfel)
    obj_gipfel.each do |gipfel|
      db.execute "INSERT INTO gipfel VALUES (?,?,?,?,? ,?,?,?,?,? ,?,?)", gipfel["gipfel_ID"], gipfel["gipfelnr"], gipfel["gipfelname_d"], gipfel["gipfelname_cz"], gipfel["status"], gipfel["typ"], gipfel["vgrd"], gipfel["ngrd"], gipfel["posfehler"], gipfel["schartenhoehe"], gipfel["talhoehe"], gipfel["sektorid"]
    end
    # WEGE
    uri_wege = URI("http://db-sandsteinklettern.gipfelbuch.de/jsonwege.php?app=yacguide&sektorid=" + gebiet["sektor_ID"])
    response_wege = Net::HTTP.get(uri_wege)
    obj_wege = JSON.parse(response_wege)
    obj_wege.each do |weg|
      db.execute "INSERT INTO wege VALUES (?,?,?,?,? ,?,?,?,?,? ,?,?,?,?)", weg["weg_ID"], weg["gipfelid"], weg["schwierigkeit"], weg["erstbegvorstieg"], weg["erstbegnachstieg"], weg["erstbegdatum"], weg["ringzahl"], weg["wegbeschr_d"], weg["wegbeschr_cz"], weg["kletterei"], weg["wegname_d"], weg["wegname_cz"], weg["wegstatus"], weg["wegnr"]
    end
    # KOMMENTARE
    uri_komment = URI("http://db-sandsteinklettern.gipfelbuch.de/jsonkomment.php?app=yacguide&sektorid=" + gebiet["sektor_ID"])
    response_komment = Net::HTTP.get(uri_komment)
    obj_komment = JSON.parse(response_komment)
    obj_komment.each do |kommentar|
      db.execute "INSERT INTO kommentare VALUES (?,?,?,?,? ,?,?,?,?,? ,?,?,?,?,?)",kommentar["komment_ID"], kommentar["userid"], kommentar["datum"], kommentar["adatum"], kommentar["wegid"], kommentar["sektorid"], kommentar["gebietid"], kommentar["qual"], kommentar["sicher"], kommentar["nass"], kommentar["kommentar"], kommentar["gipfelid"], kommentar["schwer"], kommentar["geklettert"], kommentar["begehung"]
    end
end


