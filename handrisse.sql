SELECT 
	gebiete.sektorname_d,
	gipfel.gipfelnr, gipfel.gipfelname_d,
	wege.wegname_d, wege.schwierigkeit, wege.kletterei, wege.wegbeschr_d,
	kommentare.kommentar
FROM wege
	INNER JOIN gipfel
	ON gipfel.gipfel_ID = wege.gipfelid
	INNER JOIN gebiete
	ON gebiete.sektor_ID = gipfel.sektorid
	LEFT OUTER JOIN kommentare
	ON kommentare.wegid = wege.weg_ID
WHERE wegbeschr_d LIKE '%handri%'
AND (schwierigkeit LIKE 'I %' OR schwierigkeit LIKE 'II%' OR schwierigkeit LIKE 'IV%')
ORDER BY gebiete.sektor_ID, wege.schwierigkeit